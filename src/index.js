import Web3 from "web3";
import abi from "./abi";
const ethUtil = require("ethereumjs-util");
import ethereumTransaction from "ethereumjs-tx";
const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://ropsten.infura.io/v3/e68eb31d4fa944aaa359ea31af9b0db8"
  )
);

let privateKeyBuffer = Buffer.from(
  "c0081c5c850b4f8c1522c2291d91ea0d4b6b598f3aa337963fa1bde62894ab3e",
  "hex"
);

let address = "0x" + ethUtil.privateToAddress(privateKeyBuffer).toString("hex");

var electionContract = web3.eth.contract(abi);
var election = electionContract.at(
  //"0x593f93a04c68ca3b4e57499502398350ebc0f639"
  "0x3e040ec133d84dd4904b6aa3bb7bc3a1288d588e"
);

function setEVMTime(duration) {
  return new Promise((resolve, reject) => {
    web3.currentProvider.send(
      {
        jsonrpc: "2.0",
        method: "evm_increaseTime",
        params: [duration],
        id: new Date().getSeconds()
      },
      err1 => {
        if (err1) return reject(err1);
        web3.currentProvider.send(
          {
            jsonrpc: "2.0",
            method: "evm_mine",
            params: [duration],
            id: new Date().getSeconds()
          },
          (err2, res) => {
            return err2 ? reject(err2) : resolve(res);
          }
        );
      }
    );
  });
}

function sendTransactionToSmartContract(serializedTransaction) {
  return new Promise((resolve, reject) => {
    web3.eth.sendRawTransaction(
      "0x" + serializedTransaction.toString("hex"),
      function(err, hash) {
        if (!err) {
          console.log(hash);
          resolve(hash);
        } else {
          console.log(err);
          reject(err);
        }
      }
    );
  });
}

async function transaction(dataField) {
  const transactionParams = {
    from: address,
    nonce: web3.toHex(await web3.eth.getTransactionCount(address)),
    gasPrice: web3.toHex(await web3.eth.gasPrice.toString()),
    gasLimit: web3.toHex(300000),
    to: "0x3e040ec133d84dd4904b6aa3bb7bc3a1288d588e",
    value: "0x0",
    data: dataField,
    chainId: 3
  };

  const transaction = new ethereumTransaction(transactionParams);
  transaction.sign(privateKeyBuffer);
  const serializedTransaction = transaction.serialize();
  //console.log(serializedTransaction);
  await sendTransactionToSmartContract(serializedTransaction);
}

function setCandidateRegistrationPeriod(startTime, endTime) {
  return election.setCandidateRegistrationPeriod.getData(
    startTime,
    endTime
  );
}

function registerCandidates(name, age) {
  return election.registerCandidate.getData(name, age);
}

async function setVotePeriod(startTime, endTime) {
  return await election.setVotingPeriod.getData(startTime, endTime);
}

function addVoters(address) {
  return election.addWhitelisted.getData(address);
}

function giveVote(voterId, candidateId) {
  return election.vote.getData(voterId, candidateId);
}

function setResultDay(day) {
  return election.setResultDeclationDay.getData(day);
}

function electionResult() {
  return election.maintainVotingCount.getData();
}

var electionContractCalling = async () => {
  let setCandidateRegPeriod = await setCandidateRegistrationPeriod(
    1551761662,
    1552020862
  ); 

  //await transaction(setCandidateRegPeriod);

  let startTime = await election.startCandidateRegistrationTime.call();
  console.log(startTime.toNumber()); 

  await setEVMTime(1551790462);
  let candidate1 = await registerCandidates("sam", 22);
  //await transaction(candidate1);

  let candidateId = await election.getCandidate.call(1);
  console.log(candidateId.toNumber()); 
  
  let candidate2 = await registerCandidates("div", 42);
  //transaction(candidate2);
  
  let candidate3 = await registerCandidates("john", 32);
  //transaction(candidate3);
 

  let voter1 = await addVoters(web3.eth.accounts[1]);
  //await transaction(voter1);
  
  let voter2 = await addVoters(web3.eth.accounts[2]);
  //transaction(voter2);
  
  let voter3 = await addVoters(web3.eth.accounts[3]);
  //transaction(voter3);
 

  let votionPeriod = await setVotePeriod(1551615369, 1551788169);
  //await transaction(votionPeriod);
  
  //await setEVMTime(1552136062);

  
  let vote1 = await giveVote(web3.eth.accounts[1], candidateId.toNumber());
  //await transaction(vote1);
 
  let vote2 = await giveVote(web3.eth.accounts[2], 1);
  //transaction(vote2);
 
  let resultDay = await setResultDay(1552654462);
  //await transaction(resultDay);
  
  //await setEVMTime(1552665262);

  let result = await electionResult();
  
  console.log("Candidate " + result + " has won.");
  //await transaction(result);
};

electionContractCalling();
